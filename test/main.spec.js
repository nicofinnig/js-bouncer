//import { describe, it } from "mocha";
const { describe, it } = require('mocha');
//import { expect } from "chai";
const { expect } = require('chai');
//import main from "../src/main.js";
const main = require('../src/main');
let output = "";
let log = console.log;


describe("Test suite", () => {
    before(() => {
        console.log = (content) => {
            output += content + '\n';
        };
    });
    beforeEach(() => {
        output = "";
    });
    it('No identifier', () =>{
        main(["node", "bouncer.js"]);
        //log(output);
        expect(output).to.equal("No identifier, insert age.\nYou are not allowed to enter!\n");
    });
    it('Faulty identifier', () => {
        // ##input##
        main(["node", "bouncer.js", "hello"]);
        // ##output##
        // "Invalid identifier."
        // "You are not allowed to enter!"
        expect(output).to.eq("Invalid identifier.\nYou are not allowed to enter!\n");
    });

    it('Underage', () => {
        // ##input##
        main(["node", "bouncer.js", "17"]);
        // ##output##
        // "Under age."
        // "You are not allowed to enter!"
        expect(output).to.eq("Under age.\nYou are not allowed to enter!\n");
    });
    
    it('Legal age', () => {
        // ##input##
        main(["node", "bouncer.js", "18"]);
        // "18"
        // ##output##
        expect(output).to.eq("Welcome!\n");
    });
});
