// NodeJS
const welcome = () => console.log('Welcome!');
const deny = () => console.log('You are not allowed to enter!');

/**
 * 
 * @param {Array<string>} args 
 */
const main = (args) => {
    if (args.length < 3) {
        console.log("No identifier, insert age.");
        deny();
    } else {
        try {
            const age = parseInt(args[2], 10);
            if (isNaN(age)) throw new Error();
            if (age >= 18) {
                welcome();
            } else {
                console.log("Under age.");
                deny();
            }
        } catch (err) {
            console.log("Invalid identifier.");
            deny();
        }
    }
};

// hack around to run code if "main"
// equivalent to:
// Python: if __name__ == '__main__':
// CommonJS:
if (require.main == module) {
     main(process.argv);
}
module.exports = main;

// EcmaScript
// import * as url from 'node:url';

// if (import.meta.url.startsWith('file:')) { // (A)
//   const modulePath = url.fileURLToPath(import.meta.url);
//   if (process.argv[1] === modulePath) { // (B)
//     main(process.argv);
//   }
// }

// export default main;
