#!/bin/bash

git init
echo "node_modules" > .gitignore
echo ".nyc_output" >> .gitignore
echo ".DS_Store" >> .gitignore

npm init -y
npm pkg set type='module'
npm i --save-dev mocha chai nyc
npm pkg set scripts.start='node main.js'
npm pkg set scripts.test='mocha'
npm pkg set scripts.coverage='nyc npm run test'

# change repository address
git remote set-url origin https://example.com/group/project_name
git push -u origin main

# reinstall dependencies
npm i

# change repository address
git remote set-url origin https://example.com/group_or_username/project_name

# reinstall dependecies
npm i
